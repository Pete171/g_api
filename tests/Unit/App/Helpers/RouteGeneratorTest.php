<?php

namespace Tests\Unit\App\Helpers;

use PHPUnit\Framework\TestCase;
use App\Helpers\RouteGenerator;

class RouteGeneratorTest extends TestCase
{
    public function setUp()
    {
        $allowedActions = [
            'getSingle' => [
                'httpVerb' => 'get',
                'routePattern' => '<plural>/{<singular>}'
            ],
            'getList' => [
                'httpVerb' => 'get',
                'routePattern' => '<plural>',
            ],
            'create' => [
                'httpVerb' => 'post',
                'routePattern' => '<plural>',
            ],
            'update' => [
                'httpVerb' => 'put',
                'routePattern' => '<plural>/{<singular>}'
            ],
        ];

        $this->_routeGenerator = $this->getMockBuilder(RouteGenerator::class)
                               ->setMethods(['addRoute'])
                               ->setConstructorArgs([$allowedActions])
                               ->getMock();
    }

    /**
      * @expectedException \Exception
      * @expectedExceptionMessage httpVerb missing from allowed action getSomething.
      */
    public function testConstructor_WhenMissingHttpVerbInAllowedActions() : void
    {
        new RouteGenerator([
            'getSingle' => [
                'httpVerb' => 'get',
                'routePattern' => '<plural>/{<singular>}'
            ],
            'getSomething' => [
                'routePattern' => '<plural>/{<singular>}'
            ],
        ]);
    }

    /**
      * @expectedException \Exception
      * @expectedExceptionMessage routePattern missing from allowed action getSomething.
      */
    public function testConstructor_WhenMissingRoutePatternInAllowedActions() : void
    {
        new RouteGenerator([
            'getSingle' => [
                'httpVerb' => 'get',
                'routePattern' => '<plural>/{<singular>}'
            ],
            'getSomething' => [
                'httpVerb' => 'get',
            ],
        ]);
    }

    public function testBuildRoutes_WhenWorks() : void
    {
        $modelTypes = [
            'recipes' => [
                'pluralName' => 'recipes',
                'singularName' => 'recipe',
                'controllerName' => 'RecipeController',
                'allowedActions' => [
                    'getSingle',
                    'getList',
                    'create',
                    'update',
                ],
            ],
            'ratings' => [
                'pluralName' => 'ratings',
                'singularName' => 'rating',
                'controllerName' => 'RatingController',
                'allowedActions' => [
                    'getSingle',
                    'getList',
                    'create',
                    'update',
                ],
            ],
        ];

        $this->_routeGenerator
            ->expects($this->exactly(8))
            ->method('addRoute')
            ->withConsecutive(
                ['get', 'recipes/{recipe}', 'RecipeController@getSingle'],
                ['get', 'recipes', 'RecipeController@getList'],
                ['post', 'recipes', 'RecipeController@create'],
                ['put', 'recipes/{recipe}', 'RecipeController@update'],
                ['get', 'ratings/{rating}', 'RatingController@getSingle'],
                ['get', 'ratings', 'RatingController@getList'],
                ['post', 'ratings', 'RatingController@create'],
                ['put', 'ratings/{rating}', 'RatingController@update']
            );

        $this->_routeGenerator->buildRoutes($modelTypes);
    }

    /**
      * @expectedException \Exception
      * @expectedExceptionMessage No allowedActions in model type recipes.
      */
    public function testBuildRoutes_WhenNoAllowedActions() : void
    {
        $modelTypes = [
            'recipes' => [],
        ];

        $this->_routeGenerator->buildRoutes($modelTypes);
    }

    /**
      * @expectedException \Exception
      * @expectedExceptionMessage Action unrecognisedAction is not in the Controller Action - HTTP Verb mapping.
      */
    public function testBuildRoutes_WhenUnrecognisedAllowedAction() : void
    {
        $modelTypes = [
            'recipes' => [
                'pluralName' => 'recipes',
                'singularName' => 'recipe',
                'controllerName' => 'RecipeController',
                'allowedActions' => [
                    'getSingle',
                    'getList',
                    'unrecognisedAction',
                ],
            ],
        ];

        $this->_routeGenerator->buildRoutes($modelTypes);
    }
}
