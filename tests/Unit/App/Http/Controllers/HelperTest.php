<?php

namespace Tests\Unit\App\Http\Controllers;

use App\Http\Controllers\Helper;

class HelperTest extends \PHPUnit\Framework\TestCase
{
    public function setUp()
    {
        $this->_helper = new Helper();
    }
    
    public function testGetSingle() : void
    {
        $modelMock = $this->createMock('\Illuminate\Database\Eloquent\Model');
        $returnValue = $this->_helper->getSingle($modelMock);
        $this->assertSame($modelMock, $returnValue);
    }
    /*
    public function testGetList()
    {
        $requestMock = $this->createMock('Illuminate\Http\Request');
        $requestMock
            ->expects($this->once())
            ->method('query')
            ->with($this->equalTo('all'))
            ->willReturn(true);

        $collectionMock = $this->createMock('\Illuminate\Database\Eloquent\Collection');
        $collectionMock
            ->expects($this->once())
            ->method('toArray')
            ->willReturn(['dummy_model_1', 'dummy_model_2']);
#TODO-fix this test
        $modelMock = $this->getMockBuilder('\Illuminate\Database\Eloquent\Model')->setMethods(['all'])->getMock();
        $modelMock
            ->expects($this->once())
            ->method('all')
            ->willReturn($collectionMock);

        $returnValue = $this->_helper->getList($requestMock, $modelMock);
        $this->assertEquals(['dummy_model_1', 'dummy_model_2'], $returnValue);
    }*/
    /*
    public function testCreate()
    {
        #$requestMock = $this->createMock('Illuminate\Http\Request');
        $requestMock = $this->getMockBuilder('Illuminate\Http\Request')->setMethods(['all', 'validate'])->getMock();
        $requestMock
            ->expects($this->once())
            ->method('all')
            ->willReturn(['key1' => 'value1', 'key2' => 'value2']);

        $modelMock = $this->createMock('\Illuminate\Database\Eloquent\Model');
        $modelMock
            ->expects($this->once())
            ->method('fill')
            ->with(['key1' => 'value1', 'key2' => 'value2']);

        $modelMock
            ->expects($this->once())
            ->method('save');

        $returnValue = $this->_helper->getList($requestMock, $modelMock);
        $this->assertSame($modelMock, $returnValue);
    } #TODO-also broken...
    */
}
