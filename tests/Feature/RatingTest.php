<?php

namespace Tests\Feature;

class RatingTest extends TestAbstract
{
    public function testGetRatings()
    {
        $expectedJson = [
            'error' => 0,
            'data' => [
                [
                    'id' => 1,
                    'created_at' => $this->_currentDateTime,
                    'updated_at' => $this->_currentDateTime,
                    'user_id' => '9',
                    'recipe_id' => '1',
                    'rating' => '5',
                ],
                [
                    'id' => 2,
                    'created_at' => $this->_currentDateTime,
                    'updated_at' => $this->_currentDateTime,
                    'user_id' => '9',
                    'recipe_id' => '3',
                    'rating' => '2',
                ],
            ],
        ];

        $this->_getAllRatings()
            ->assertStatus(200)
            ->assertExactJson($expectedJson);
    }

    public function testAddRating()
    {
        $this->_assertTotalRatingsCount(2);

        $newRatingData = [
            'recipe_id' => 8,
            'rating' => 4,
            'user_id' => 9,
        ];
        
        $responseJson = $this->_addRating($newRatingData)
            ->assertStatus(201)
            ->decodeResponseJson();

        $this->_assertTotalRatingsCount(3);

        $responseJson = $this->_handleModelTimestampsAfterInsert($responseJson);
        
        $expectedJson = [
            'error' => 0,
            'data' => [
                'id' => 3,
                'recipe_id' => 8,
                'rating' => 4,
                'user_id' => 9,
            ],
        ];

        $this->assertEquals($expectedJson, $responseJson);
    }

    /**
     * @dataProvider providerAddRating_WhenRatingInvalid
     */
    public function testAddRating_WhenRatingInvalid($newRating, array $expectedValidationErrors)
    {
        $this->_assertTotalRatingsCount(2);

        $newRatingData = [
            'recipe_id' => 8,
            'rating' => $newRating,
            'user_id' => 9,
        ];
        
        $jsonResponse = $this->_addRating($newRatingData)
                      ->assertStatus(422)
                      ->decodeResponseJson();

        $this->_assertValidationErrorsInResponse($jsonResponse, $expectedValidationErrors);
        
        $this->_assertTotalRatingsCount(2);
    }

    public function providerAddRating_WhenRatingInvalid()
    {
        return [
            [
                'notAnInt',
                ['rating' => ['The rating must be a number.']],
            ],
            [
                6,
                ['rating' => ['The rating must be between 0 and 5.']],
            ],
            [
                -1,
                ['rating' => ['The rating must be between 0 and 5.']],
            ],                        
        ];
    }
}
