<?php

#TODO - review all test cases: add failure cases
#TODO-request validation in controller
#TODO-update should have 202 updated status?
//TODO-see vendor/laravel/framework/src/Illuminate/Routing/Router.php and grep for 201 re. addHTTP status

namespace Tests\Feature;

use Tests\TestCase;
use Illuminate\Foundation\Testing\WithFaker;
use Illuminate\Foundation\Testing\RefreshDatabase;

class RecipeTest extends TestAbstract
{

    public function testGetRecipeById()
    {
        $expectedJson = [
            'error' => 0,
            'data' => [
                'id' => 1,
                'created_at' => '30/06/2015 17:58:00',
                'updated_at' => '30/06/2015 17:58:00',
                'box_type' => 'vegetarian',
                'title' => 'Sweet Chilli and Lime Beef on a Crunchy Fresh Noodle Salad',
                'slug' => 'sweet-chilli-and-lime-beef-on-a-crunchy-fresh-noodle-salad',
                'short_title' => '',
                'marketing_description' => 'Here we\'ve used onglet steak which is an extra flavoursome ' .
                    'cut of beef that should never be cooked past medium rare. So if you\'re a fan of well ' .
                    'done steak, this one may not be for you. However, if you love rare ' .
                    'steak and fancy trying a new cut, please be',
                'calories_kcal' => '401',
                'protein_grams' => '12',
                'fat_grams' => '35',
                'carbs_grams' => '0',
                'bulletpoint1' => '',
                'bulletpoint2' => '',
                'bulletpoint3' => '',
                'recipe_diet_type_id' => 'meat',
                'season' => 'all',
                'base' => 'noodles',
                'protein_source' => 'beef',
                'preparation_time_minutes' => '35',
                'shelf_life_days' => '4',
                'equipment_needed' => 'Appetite',
                'origin_country' => 'Great Britain',
                'recipe_cuisine' => 'asian',
                'in_your_box' => '',
                'gousto_reference' => '59',
            ],
        ];
        
        $this->_getRecipe(1)
            ->assertStatus(200)
            ->assertExactJson($expectedJson);
    }

    /**
     * @dataProvider providerGetRecipeById_WhenGivenInvalidId
     */
    public function testGetRecipeById_WhenGivenInvalidId($recipeId) : void
    {
        $jsonResponse = $this->_getRecipe($recipeId)
            ->assertStatus(404)
            ->decodeResponseJson();

        $this->_assertExceptionInResponse(
            $jsonResponse,
            '/^No query results for model \[App\\\\Models\\\\Recipe\]\.$/'
        );
    }

    public function providerGetRecipeById_WhenGivenInvalidId()
    {
        return [
            [11], // Only have IDs 1 - 10.
            ['stringInsteadOfAnInt'],
        ];
    }
    
    public function testGetAllRecipes()
    {
        $expectedJson = [
            'error' => 0,
            'data' => [
                [
                    'id' => '1',
                    'title' => 'Sweet Chilli and Lime Beef on a Crunchy Fresh Noodle Salad',
                ],
                [
                    'id' => '2',
                    'title' => 'Tamil Nadu Prawn Masala',
                ],
                [
                    'id' => '3',
                    'title' => 'Umbrian Wild Boar Salami Ragu with Linguine',
                ],
                [
                    'id' => '4',
                    'title' => 'Tenderstem and Portobello Mushrooms with Corn Polenta',
                ],
                [
                    'id' => '5',
                    'title' => 'Fennel Crusted Pork with Italian Butter Beans',
                ],
                [
                    'id' => '6',
                    'title' => 'Pork Chilli',
                ],
                [
                    'id' => '7',
                    'title' => 'Courgette Pasta Rags',
                ],
                [
                    'id' => '8',
                    'title' => 'Homemade Eggs & Beans',
                ],
                [
                    'id' => '9',
                    'title' => 'Grilled Jerusalem Fish',
                ],
                [
                    'id' => '10',
                    'title' => 'Pork Katsu Curry',
                ],
            ],
        ];

        $actualJson = $this->_getAllRecipes() #TODO-add test for ?all=0
                      ->assertStatus(200)
                      ->decodeResponseJson();
        
        $this->_assertRecipesContain($expectedJson, $actualJson);
    }

    public function testGetAllRecipes_ByCuisine()
    {
        $expectedJson = [
            'error' => 0,
            'data' => [
                [
                    'id' => '3',
                    'title' => 'Umbrian Wild Boar Salami Ragu with Linguine',
                ],
                [
                    'id' => '4',
                    'title' => 'Tenderstem and Portobello Mushrooms with Corn Polenta',
                ],
                [
                    'id' => '5',
                    'title' => 'Fennel Crusted Pork with Italian Butter Beans',
                ],
                [
                    'id' => '7',
                    'title' => 'Courgette Pasta Rags',
                ],
            ],
        ];

        $actualJson = $this->_getRecipes(['recipe_cuisine' => 'british'])
                      ->assertStatus(200)
                      ->decodeResponseJson();

        $this->_assertRecipesContain($expectedJson, $actualJson);
    }

    public function testGetAllRecipes_ByCuisine_AndBySeason()
    {
        $expectedJson = [
            'error' => 0,
            'data' => [
                [
                    'id' => '7',
                    'title' => 'Courgette Pasta Rags',
                ],
            ],
        ];

        $actualJson = $this->_getRecipes(['recipe_cuisine' => 'british', 'protein_source' => 'chicken'])
                      ->assertStatus(200)
                      ->decodeResponseJson();

        $this->_assertRecipesContain($expectedJson, $actualJson);
    }
    
    /**
     *
     */
    public function testGetRecipes_WithPagination_10RowsInto2Pages()
    {
        $expectedJson = [
            'error' => 0,
            'data' => [
                [
                    'id' => '1',
                    'title' => 'Sweet Chilli and Lime Beef on a Crunchy Fresh Noodle Salad',
                ],
                [
                    'id' => '2',
                    'title' => 'Tamil Nadu Prawn Masala',
                ],
                [
                    'id' => '3',
                    'title' => 'Umbrian Wild Boar Salami Ragu with Linguine',
                ],
                [
                    'id' => '4',
                    'title' => 'Tenderstem and Portobello Mushrooms with Corn Polenta',
                ],
                [
                    'id' => '5',
                    'title' => 'Fennel Crusted Pork with Italian Butter Beans',
                ],
            ],
        ];
        
        $actualJson = $this->_getRecipes(['recordsPerPage' => 5, 'page' => 1])
                      ->assertStatus(200)
                      ->decodeResponseJson();

        $this->_assertRecipesContain($expectedJson, $actualJson);
        
        $expectedJson = [
            'error' => 0,
            'data' => [
                [
                    'id' => '6',
                    'title' => 'Pork Chilli',
                ],
                [
                    'id' => '7',
                    'title' => 'Courgette Pasta Rags',
                ],
                [
                    'id' => '8',
                    'title' => 'Homemade Eggs & Beans',
                ],
                [
                    'id' => '9',
                    'title' => 'Grilled Jerusalem Fish',
                ],
                [
                    'id' => '10',
                    'title' => 'Pork Katsu Curry',
                ],
            ],
        ];

        $actualJson = $this->_getRecipes(['recordsPerPage' => 5, 'page' => 2])
                    ->assertStatus(200)
                    ->decodeResponseJson();

        $this->_assertRecipesContain($expectedJson, $actualJson);

        $expectedJson = [
            'error' => 0,
            'data' => [],
        ];

        $actualJson = $this->_getRecipes(['recordsPerPage' => 5, 'page' => 3])
                    ->assertStatus(200)
                    ->decodeResponseJson();
        
        $this->_assertRecipesContain($expectedJson, $actualJson);
    }

    public function testGetRecipes_WithPagination_4RowsInto3Pages()
    {
        $expectedJson = [
            'error' => 0,
            'data' => [
                [
                    'id' => '1',
                    'title' => 'Sweet Chilli and Lime Beef on a Crunchy Fresh Noodle Salad',
                ],
                [
                    'id' => '2',
                    'title' => 'Tamil Nadu Prawn Masala',
                ],
                [
                    'id' => '3',
                    'title' => 'Umbrian Wild Boar Salami Ragu with Linguine',
                ],
                [
                    'id' => '4',
                    'title' => 'Tenderstem and Portobello Mushrooms with Corn Polenta',
                ],
            ],
        ];
        
        $actualJson = $this->_getRecipes(['recordsPerPage' => 4, 'page' => 1])
                      ->assertStatus(200)
                      ->decodeResponseJson();

        $this->_assertRecipesContain($expectedJson, $actualJson);
        
        $expectedJson = [
            'error' => 0,
            'data' => [
                [
                    'id' => '5',
                    'title' => 'Fennel Crusted Pork with Italian Butter Beans',
                ],
                [
                    'id' => '6',
                    'title' => 'Pork Chilli',
                ],
                [
                    'id' => '7',
                    'title' => 'Courgette Pasta Rags',
                ],
                [
                    'id' => '8',
                    'title' => 'Homemade Eggs & Beans',
                ],
            ],
        ];

        $actualJson = $this->_getRecipes(['recordsPerPage' => 4, 'page' => 2])
                    ->assertStatus(200)
                    ->decodeResponseJson();
        
        $this->_assertRecipesContain($expectedJson, $actualJson);

        $expectedJson = [
            'error' => 0,
            'data' => [
                [
                    'id' => '9',
                    'title' => 'Grilled Jerusalem Fish',
                ],
                [
                    'id' => '10',
                    'title' => 'Pork Katsu Curry',
                ],
            ],
        ];

        $actualJson = $this->_getRecipes(['recordsPerPage' => 4, 'page' => 3])
                    ->assertStatus(200)
                    ->decodeResponseJson();
        
        $this->_assertRecipesContain($expectedJson, $actualJson);
    }
    
    /**
     * @dataProvider providerGetAllRecipes_WithInvalidData
     */
    public function testGetAllRecipes_WithInvalidPage(array $queryStringParams, array $expectedValidationErrors)
    {
        $jsonResponse = $this->_getRecipes($queryStringParams)
                      ->assertStatus(422)
                    ->decodeResponseJson();

        $this->_assertValidationErrorsInResponse($jsonResponse, $expectedValidationErrors);
    }

    public function providerGetAllRecipes_WithInvalidData()
    {
        return [
            [
                ['recordsPerPage' => 4, 'page' => 'invalidPage'],
                ['page' => ['The page must be a number.']],
            ],
            [
                ['recordsPerPage' => 4, 'page' => -5],
                ['page' => ['The page must be greater than or equal 0.']],
            ],
            [
                ['recordsPerPage' => 'invalidRecordsPerPage', 'page' => 1],
                ['recordsPerPage' => ['The records per page must be a number.']],
            ],
            [
                ['recordsPerPage' => -5, 'page' => 1],
                ['recordsPerPage' => ['The records per page must be greater than or equal 0.']],
            ],
            [
                ['recordsPerPage' => 'invalid', 'page' => 'invalid'],
                [
                    'recordsPerPage' => ['The records per page must be a number.'],
                    'page' => ['The page must be a number.'],
                ],
            ],
        ];
    }
    
    public function testAddRecipe()
    {
        $this->_assertTotalRecipesCount(10);

        $newRecipeData = [
            'title' => 'My Test Recipe',
        ];
        
        $jsonResponse = $this->_addRecipe($newRecipeData)
            ->assertStatus(201)
            ->decodeResponseJson();

        $this->_assertTotalRecipesCount(11);
        
        $jsonResponse = $this->_handleModelTimestampsAfterInsert($jsonResponse);
        
        $expectedJson = [
            'error' => 0,
            'data' => [
                'id' => 11,
                'title' => 'My Test Recipe',
            ],
        ];

        $this->assertEquals($expectedJson, $jsonResponse);
    }
        
    public function testUpdateRecipe()
    {
        $this->_assertTotalRecipesCount(10);
        
        $this->_assertRecipeWithIdContains(6, [
            'title' => 'Pork Chilli',
            'slug' => 'pork-chilli',
        ]);
    
        [$originalCreatedTime, $originalUpdatedTime] = $this->_getTimeForRecipe(6);
        
        $dataToUpdate = [
            'title' => 'RENAMED from Pork Chilli',
        ];
        
        $jsonResponse = $this->_updateRecipe(6, $dataToUpdate)
            ->assertStatus(200)
            ->decodeResponseJson();
        
        $jsonResponse = $this->_handleModelTimestampsAfterUpdate(
            $originalCreatedTime,
            $originalUpdatedTime,
            $jsonResponse
        );
        
        $this->_assertRecipeWithIdContains(6, [
            'title' => 'RENAMED from Pork Chilli',
            'slug' => 'pork-chilli',
        ]);

        $this->_assertTotalRecipesCount(10);
    }
}
