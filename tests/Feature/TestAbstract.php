<?php

namespace Tests\Feature;

use Tests\TestCase;

abstract class TestAbstract extends TestCase
{
    public function setUp()
    {
        parent::setUp();
        
        $this->_envFileContents = "
APP_NAME=Laravel
APP_ENV=local
APP_KEY=
APP_DEBUG=true
APP_URL=http://localhost

LOG_CHANNEL=stack

DB_CONNECTION=sqlite
DB_DATABASE=/tmp/database.db

BROADCAST_DRIVER=log
CACHE_DRIVER=file
SESSION_DRIVER=file
SESSION_LIFETIME=120
QUEUE_DRIVER=sync

REDIS_HOST=127.0.0.1
REDIS_PASSWORD=null
REDIS_PORT=6379

MAIL_DRIVER=smtp
MAIL_HOST=smtp.mailtrap.io
MAIL_PORT=2525
MAIL_USERNAME=null
MAIL_PASSWORD=null
MAIL_ENCRYPTION=null

PUSHER_APP_ID=
PUSHER_APP_KEY=
PUSHER_APP_SECRET=
PUSHER_APP_CLUSTER=mt1

MIX_PUSHER_APP_KEY=\"\${PUSHER_APP_KEY}\"
MIX_PUSHER_APP_CLUSTER=\"\${PUSHER_APP_CLUSTER}\"
";
        
        file_put_contents('/tmp/database.db', '');
  
        $this->_execCmd('php artisan migrate');

        $this->_rebuildEnvTestingFile();
        $this->_initPdo();

        $this->_currentDateTime = (new \DateTime())->format('d/m/Y H:i:s');
        
        $this->_insertRecipeFileIntoDatabase();
        $this->_assertRecipesRowCount(10);
        
        $this->_query(
            '' .
                'INSERT INTO ratings (recipe_id, rating, user_id, created_at, updated_at) ' .
                'VALUES (1, 5, 9, :date, :date), (3, 2, 9, :date, :date)' .
            '',
            ['date' => $this->_currentDateTime]
        );

        $this->_assertRatingsRowCount(2);
    }

    protected function _rebuildEnvTestingFile()
    {
        unlink(realpath(__DIR__ . '/../../') . '/.env.testing');
        file_put_contents(realpath(__DIR__ . '/../../') . '/.env.testing', $this->_envFileContents);
    }
    
    protected function _initPdo()
    {
        $this->_pdo = new \PDO(
            'sqlite:/tmp/database.db',
            null,
            null,
            [\PDO::ATTR_PERSISTENT => true]
        );
    }

    protected function _execCmd(string $cmd, int $expectedReturnStatus = 0) : array
    {
        $output = null;
        $returnStatus = null;
        
        exec($cmd, $output, $returnStatus);
        
        if ($returnStatus !== $expectedReturnStatus) {
            throw new \Exception(sprintf('Failed to execute command %s.  Output: %s.', $cmd, print_r($output, 1)));
        }

        return $output;
    }
    
    protected function _insertRecipeFileIntoDatabase()
    {
        $recipeFile = __DIR__ . '/helpers/recipe-data.csv';
        if (false === ($file = fopen($recipeFile, 'r'))) {
            throw new \Exception(sprintf('Unable to read recipe file %s.', $recipeFile));
        }

        if (false === ($header = fgetcsv($file, 1000, ","))) {
            throw new \Exception(sprintf('Unable to parse header line from file %s.', $recipeFile));
        }
        
        while (false !== ($data = fgetcsv($file, 1000, ","))) {
            $query = sprintf(
                'INSERT INTO recipes VALUES (%s)',
                implode(', ', array_fill(0, count($header), '?'))
            );
            $statement = $this->_query($query, $data);
        }
        
        fclose($file);
    }
    
    protected function _query(string $query, array $params = []) : \PDOStatement
    {
        if (false === ($statement = $this->_pdo->prepare($query))) {
            throw new \Exception(sprintf(
                'Failed to prepare query %s with error information %s',
                $query,
                print_r($this->_pdo->errorInfo(), 1)
            ));
        }
        
        if (false === $statement->execute($params)) {
            var_dump($query, $params);
            throw new \Exception(sprintf(
                'Failed to execute query %s with error information %s',
                $query,
                print_r($this->_pdo->errorInfo(), 1)
            ));
        }
        
        return $statement;
    }

    protected function _getRecipe(string $id)
    {
        return $this->json('GET', sprintf('/api/recipes/%s', $id), []);
    }
    
    protected function _getAllRecipes(string $queryString = '')
    {
        return $this->_getRecipes(['all' => 1]);
    }
    
    protected function _getRecipes(array $queryStringData)
    {
        $urlPath = sprintf('/api/recipes?%s', http_build_query($queryStringData));
        return $this->json('GET', $urlPath, []);
    }

    protected function _addRecipe(array $dataToAdd)
    {
        return $this->json('POST', '/api/recipes', $dataToAdd);
    }

    protected function _updateRecipe(int $id, array $dataToUpdate)
    {
         //[ // TODO - id could be updated! fix...
        return $this->json('PUT', sprintf('/api/recipes/%s', $id), $dataToUpdate);
    }

    protected function _getAllRatings()
    {
        return $this->json('GET', '/api/ratings', []);
    }

    protected function _addRating(array $dataToAdd)
    {
        return $this->json('POST', '/api/ratings', $dataToAdd);
    }

    protected function _handleModelTimestampsAfterInsert(array $responseJson)
    {
        $createdAt = strtotime($responseJson['data']['created_at']);
        $updatedAt = strtotime($responseJson['data']['updated_at']);
        $oldDateTime = strtotime($this->_currentDateTime);
        
        $this->assertNotFalse($createdAt);
        $this->assertNotFalse($updatedAt);
        $this->assertNotFalse($oldDateTime);
        
        $this->assertGreaterThanOrEqual($oldDateTime, $createdAt);
        $this->assertGreaterThanOrEqual($oldDateTime, $updatedAt);
            
        unset($responseJson['data']['created_at']);
        unset($responseJson['data']['updated_at']);

        return $responseJson;
    }

    protected function _handleModelTimestampsAfterUpdate(
        string $originalCreatedTime,
        string $originalUpdatedTime,
        array $responseJson
    ) : array {
        $createdAt = strtotime($responseJson['data']['created_at']);
        $updatedAt = strtotime($responseJson['data']['updated_at']);
        $originalCreatedTimestamp = strtotime($originalCreatedTime);
        $originalUpdatedTimestamp = strtotime($originalUpdatedTime);
        
        $this->assertNotFalse($createdAt);
        $this->assertNotFalse($updatedAt);
        $this->assertNotFalse($originalCreatedTimestamp);
        $this->assertNotFalse($originalUpdatedTimestamp);
        
        $this->assertEquals($originalCreatedTimestamp, $createdAt);
        $this->assertGreaterThanOrEqual($originalUpdatedTimestamp, $updatedAt);
            
        unset($responseJson['data']['created_at']);
        unset($responseJson['data']['updated_at']);

        return $responseJson;
    }

    protected function _getTimeForRecipe($id)
    {
        $json = $this->_getRecipe($id)
              ->assertStatus(200)
              ->decodeResponseJson();

        $this->assertEquals(0, $json['error']);
        $this->assertArrayHasKey('data', $json);
            
        return [$json['data']['created_at'], $json['data']['updated_at']];
    }
        
    protected function _assertRecipesRowCount(int $expectedCount) : void
    {
        $statement = $this->_query('SELECT COUNT(*) FROM recipes');
        $rowCount = (int) $statement->fetch(\PDO::FETCH_NUM)[0];
        $this->assertEquals($expectedCount, $rowCount);
    }

    protected function _assertRatingsRowCount(int $expectedCount) : void
    {
        $statement = $this->_query('SELECT COUNT(*) FROM ratings');
        $rowCount = (int) $statement->fetch(\PDO::FETCH_NUM)[0];
        $this->assertEquals($expectedCount, $rowCount);
    }

    protected function _assertTotalRecipesCount(int $expectedCount)
    {
        $jsonRecipes = $this->_getAllRecipes()
                     ->assertStatus(200)
                     ->decodeResponseJson();

        $this->assertEquals(0, $jsonRecipes['error']);
        $this->assertArrayHasKey('data', $jsonRecipes);
        
        $this->assertCount($expectedCount, $jsonRecipes['data']);
    }

    protected function _assertTotalRatingsCount(int $expectedCount)
    {
        $jsonRecipes = $this->_getAllRatings()
                     ->assertStatus(200)
                     ->decodeResponseJson();

        $this->assertEquals(0, $jsonRecipes['error']);
        $this->assertArrayHasKey('data', $jsonRecipes);
        
        $this->assertCount($expectedCount, $jsonRecipes['data']);
    }
    
    protected function _assertRecipeWithIdContains(int $id, array $dataToCheck)
    {
        $actualJson = $this->_getRecipe($id)
                    ->assertStatus(200)
                    ->decodeResponseJson();

        $this->assertEquals(0, $actualJson['error']);
        $this->assertArrayHasKey('data', $actualJson);
        
        $actualJson = array_intersect_key($actualJson['data'], $dataToCheck);
        $this->assertEquals($actualJson, $dataToCheck);
    }

    protected function _assertRecipeWithIdExists(int $recipeIdToFind) : void
    {
        $returnedJson = $this->_getAllRecipes()
                      ->assertStatus(200)
                      ->decodeResponseJson();

        $this->assertEquals(0, $returnedJson['error']);
        $this->assertArrayHasKey('data', $returnedJson);
        
        $found = false;
        foreach ($returnedJson['data'] as $recipe) {
            $recipeIdToFind = (string) $recipeIdToFind;
            if ($recipe['id'] === $recipeIdToFind) {
                $found = true;
                break;
            }
        }
        
        $this->assertTrue($found);
    }

    protected function _assertRecipeWithIdDoesNotExist(int $recipeIdToFind) : void
    {
        $returnedJson = $this->_getAllRecipes()
                      ->assertStatus(200)
                      ->decodeResponseJson();

        $this->assertEquals(0, $returnedJson['error']);
        $this->assertArrayHasKey('data', $returnedJson);
        
        $found = false;
        foreach ($returnedJson['data'] as $recipe) {
            $recipeIdToFind = (string) $recipeIdToFind;
            if ($recipe['id'] === $recipeIdToFind) {
                $found = true;
                break;
            }
        }
        
        $this->assertFalse($found);
    }

    protected function _assertRecipesContain(array $expectedJson, array $actualJson)
    {
        $this->assertEquals(0, $actualJson['error']);
        $this->assertArrayHasKey('data', $actualJson);
        
        $actualJsonFiltered = $actualJson;
        
        foreach ($actualJson['data'] as $index => $actualRecipe) {
            $this->assertArrayHasKey($index, $expectedJson['data']);
            $actualJsonFiltered['data'][$index] = array_intersect_key($actualRecipe, $expectedJson['data'][$index]);
        }
        
         $this->assertEquals($expectedJson, $actualJsonFiltered);
    }
    
    protected function _assertExceptionInResponse(array $jsonResponse, string $exceptionMessageRegex)
    {
        $this->assertEquals(1, $jsonResponse['error']);
        $this->assertArrayHasKey('data', $jsonResponse);
        $this->assertArrayHasKey('exception', $jsonResponse['data']);
        $this->assertArrayHasKey('message', $jsonResponse['data']);
        $this->assertRegExp($exceptionMessageRegex, $jsonResponse['data']['message']);
    }
    //TODO-give proper consistency to 'error' cases and find a way of having an error code that clients could parse.
    protected function _assertValidationErrorsInResponse(array $jsonResponse, array $expectedValidationErrors)
    {
        $this->assertEquals(1, $jsonResponse['error']);
        $this->assertArrayHasKey('data', $jsonResponse);
        $this->assertEquals($expectedValidationErrors, $jsonResponse['data']);
    }
}
