<?php

function initPdo(string $dbFile) : \PDO
{
    return new \PDO(
        $dbFile,
        null,
        null,
        [\PDO::ATTR_PERSISTENT => true]
    );
}

function insertRecipeFileIntoDatabase(\PDO $pdo, string $recipeFile) : void
{
    if (false === ($file = fopen($recipeFile, 'r'))) {
        throw new \Exception(sprintf('Unable to read recipe file %s.', $recipeFile));
    }

    if (false === ($header = fgetcsv($file, 1000, ","))) {
        throw new \Exception(sprintf('Unable to parse header line from file %s.', $recipeFile));
    }

    while (false !== ($data = fgetcsv($file, 1000, ","))) {
        $query = sprintf(
            'INSERT INTO recipes VALUES (%s)',
            implode(', ', array_fill(0, count($header), '?'))
        );
        $statement = query($pdo,$query, $data);
    }

    fclose($file);
}


function query(\PDO $pdo, string $query, array $params = []) : \PDOStatement
{
    if (false === ($statement = $pdo->prepare($query))) {
        throw new \Exception(sprintf(
            'Failed to prepare query %s with error information %s',
            $query,
            print_r($pdo->errorInfo(), 1)
        ));
    }

    if (false === $statement->execute($params)) {
        throw new \Exception(sprintf(
            'Failed to execute query %s with error information %s',
            $query,
            print_r($pdo->errorInfo(), 1)
        ));
    }

    return $statement;
}

$recipeFile = __DIR__ . '/tests/Feature/helpers/recipe-data.csv';
$dbFile = 'sqlite:/tmp/database.prod.db';

$pdo = initPdo($dbFile);

query($pdo, "DELETE FROM recipes");
query($pdo, "DELETE FROM ratings");

insertRecipeFileIntoDatabase($pdo, $recipeFile);
$stmt = query($pdo, "SELECT COUNT(*) FROM recipes");