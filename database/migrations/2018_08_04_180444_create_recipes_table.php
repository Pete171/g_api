<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateRecipesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('recipes', function (Blueprint $table) {
            $table->increments('id');
            $table->datetime('created_at');
            $table->datetime('updated_at');
            #$table->timestamps();
            $table->string('box_type')->default('');
            $table->string('title')->default('');
            $table->string('slug')->default('');
            $table->string('short_title')->default('');
            $table->string('marketing_description')->default('');
            $table->integer('calories_kcal')->default(0);
            $table->integer('protein_grams')->default(0);
            $table->integer('fat_grams')->default(0);
            $table->integer('carbs_grams')->default(0);
            $table->string('bulletpoint1')->default('');;
            $table->string('bulletpoint2')->default('');;
            $table->string('bulletpoint3')->default('');;
            $table->integer('recipe_diet_type_id')->default(0);
            $table->string('season')->default('');;
            $table->string('base')->default('');;
            $table->string('protein_source')->default('');;
            $table->integer('preparation_time_minutes')->default(0);
            $table->integer('shelf_life_days')->default(0);
            $table->string('equipment_needed')->default('');;
            $table->string('origin_country')->default('');;
            $table->string('recipe_cuisine')->default('');;
            $table->string('in_your_box')->default('');;
            $table->integer('gousto_reference')->default(0);
        });

        Schema::create('ratings', function (Blueprint $table) {
            $table->increments('id');
            $table->datetime('created_at');
            $table->datetime('updated_at');
            $table->integer('user_id');
            $table->integer('recipe_id');
            $table->integer('rating');
            $table->foreign('recipe_id')->references('id')->on('recipes')->onDelete('cascade');
        });        
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('recipes');
    }
}
