<?php

namespace App\Providers;

use Illuminate\Support\ServiceProvider;

class MyTestServiceProvider extends ServiceProvider
{
    /**
     * Bootstrap services.
     *
     * @return void
     */
    public function boot()
    {
        //
    }

    /**
     * Register services.
     *
     * @return void
     */
    public function register()
    {
        /*
        $this->app->when(\App\Models\RecipeFile::class)
            ->needs('$recipeFilePath')
            ->give(env('RECIPE_FILE_PATH', ''));#TODO-vlaidate enot empty
        */
        /*
        $this->app->when(\App\Http\Controllers\RecipeController::class)
        #$this->app->when('App\Http\Controllers\RecipeController')
            ->needs('$object')
            ->give('abc');
            #->give($this->app->make('\App\Models\Recipe'));
        \Illuminate\Support\Facades\Log::alert('here');

        $this->app->when(\App\Http\Controllers\ApiController::class)
        #$this->app->when('App\Http\Controllers\RecipeController')
            ->needs('$object')
            ->give('abc');
            #->give($this->app->make('\App\Models\Recipe'));

        #\Illuminate\Support\Facades\Log::alert('here');
        */
    }
}
