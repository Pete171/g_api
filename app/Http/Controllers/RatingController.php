<?php

namespace App\Http\Controllers;

use App\Models\Rating;
use Illuminate\Http\Request;

class RatingController extends ApiController
{
    public function getList(Request $request, Rating $rating)
    {
        return $this->_helper->getList($request, $rating);
    }

    public function create(Request $request, Rating $rating)
    {
        $validatedData = $request->validate([
            'rating' => 'bail|numeric|between:0,5',
        ]);
        
        return $this->_helper->create($request, $rating);
    }
}
