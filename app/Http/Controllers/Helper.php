<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

class Helper
{
    public function getSingle(\Illuminate\Database\Eloquent\Model $object) : \Illuminate\Database\Eloquent\Model
    {
        return $object;
    }
    
    public function getList(Request $request, \Illuminate\Database\Eloquent\Model $object) : array
    {
        $validatedData = $request->validate([
            'page' => 'bail|numeric|gte:0',
            'recordsPerPage' => 'bail|numeric|gte:1',
        ]);
        
        $collection = $object->all();

        $allowedFilterFields = array_keys(array_diff_key($request->query(), array_flip(['page', 'recordsPerPage', 'all'])));#TODO-allow models to specify keys that cant be filtered by
        
        foreach ($allowedFilterFields as $fieldKey) {
            $fieldValue = $request->query($fieldKey);
            if ($fieldValue) {
                $collection = $collection->filter(function ($object) use ($fieldKey, $fieldValue) {
                    return $object->getAttribute($fieldKey) === $fieldValue;
                })->values();
            }
        }
        
        if ($request->query('all')) {
            return $collection->toArray();
        }

        $recordsPerPage = $request->query('recordsPerPage', 8);
        $page = $request->query('page', 1);

        return $collection
            ->forPage($page, $recordsPerPage)
            ->values()
            ->toArray();
    }

    public function create(Request $request, \Illuminate\Database\Eloquent\Model $object)
    {
        $requestData = $request->all();
        $object->fill($requestData);
        $object->save();
        return $object;
    }

    public function update(Request $request, \Illuminate\Database\Eloquent\Model $object)
    {
        $requestData = $request->all();
        $object->fill($requestData);
        $object->save();
        return $object;
    }
}
