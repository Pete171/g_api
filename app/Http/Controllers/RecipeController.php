<?php

namespace App\Http\Controllers;

use App\Models\Recipe;
use Illuminate\Http\Request;

class RecipeController extends ApiController
{

    public function getSingle(Recipe $recipe)
    {
        return $this->_helper->getSingle($recipe);
    }

    public function getList(Request $request, Recipe $recipe)
    {
        return $this->_helper->getList($request, $recipe);
    }

    public function create(Request $request, Recipe $recipe)
    {
        return $this->_helper->create($request, $recipe);
    }

    public function update(Request $request, Recipe $recipe)
    {
        return $this->_helper->update($request, $recipe);
    }
}
