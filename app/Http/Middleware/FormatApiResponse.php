<?php

namespace App\Http\Middleware;

use Closure;
use Illuminate\Http\Request;

class FormatApiResponse
{
    public function handle($request, Closure $next)
    {
        $response = $next($request);
        
        $data = $response->getData();

        /* TODOPB - If the explicit Exception and validation handling in app/Exceptions/Handler.php
         * has run this will evaluate to false.  This is not future proof: if an update adds logic
         * for exception handling that bypasses our handling we will incorrectly be setting 'error'
         * to '0' in some cases.
         */
        if (!array_key_exists('error', $data)) {
            $response->setData(['error' => 0, 'data' => $data]);
        }
        
        return $response;
    }
}
