<?php

namespace App\Helpers;

class RouteGenerator
{
    public function __construct(array $allowedActions)
    {
        $this->_allowedActions = $allowedActions;
        $this->validateAllowedActions();
    }
    
    public function buildRoutes(array $modelTypes) : void
    {
        foreach ($modelTypes as $modelType => $routingInformation) {
            if (!array_key_exists('allowedActions', $routingInformation)) {
                throw new \Exception(sprintf('No allowedActions in model type %s.', $modelType));
            }
            foreach ($routingInformation['allowedActions'] as $allowedAction) {
                if (!array_key_exists($allowedAction, $this->_allowedActions)) {
                    throw new \Exception(
                        sprintf('Action %s is not in the Controller Action - HTTP Verb mapping.', $allowedAction)
                    );
                }
                
                $httpVerb = $this->_allowedActions[$allowedAction]['httpVerb'];
                $routePattern = $this->_allowedActions[$allowedAction]['routePattern'];
                $routePattern = preg_replace('/<plural>/', $routingInformation['pluralName'], $routePattern);
                $routePattern = preg_replace('/<singular>/', $routingInformation['singularName'], $routePattern);

                $this->addRoute(
                    $httpVerb,
                    $routePattern,
                    sprintf('%s@%s', $routingInformation['controllerName'], $allowedAction)
                );
            }
        }
    }

    protected function validateAllowedActions() : void
    {
        foreach ($this->_allowedActions as $controllerAction => $actionData) {
            if (!array_key_exists('httpVerb', $actionData)) {
                throw new \Exception(sprintf('httpVerb missing from allowed action %s.', $controllerAction));
            }
            if (!array_key_exists('routePattern', $actionData)) {
                throw new \Exception(sprintf('routePattern missing from allowed action %s.', $controllerAction));
            }
        }
    }
    
    protected function addRoute(string $httpVerb, string $routePattern, string $controller) : void
    {
        \Route::$httpVerb($routePattern, $controller);
    }
}
