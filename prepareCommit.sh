#!/bin/bash

set -e
#set -x
function warnDeveloper {
    tput setaf 1
    tput smul
    echo "Errors.  Fix before committing." >&2
    tput setaf 7
    tput rmul    
    exit 1
}

trap warnDeveloper ERR

vendor/bin/phpcs app --standard=PSR2
vendor/bin/phpcs tests --standard=PSR2 --exclude=PSR2.Methods.MethodDeclaration,PSR1.Methods.CamelCapsMethodName
vendor/bin/phpunit-randomizer --order-by=random

tput setaf 2
echo "Ready to commit."
tput setaf 7

exit 0;
