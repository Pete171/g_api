<?php

use Illuminate\Http\Request;

/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
*/

Route::middleware('auth:api')->get('/user', function (Request $request) {
    return $request->user();
});

/*

// TONOTE - These are the routes we manually would have added before \App\Helper\RouteBuilder was written.
// While simpler for this short Rest API, a real API could have had hundreds of such lines.
// This would have prevented the API from being easily scalable: it would have been more error-prone.

Route::get('recipes/{recipe}', 'RecipeController@getSingle');
Route::put('recipes/{recipe}', 'RecipeController@update');
Route::get('recipes', 'RecipeController@getList');
Route::post('recipes', 'RecipeController@create');

Route::get('ratings', 'RatingController@getList');
Route::post('ratings', 'RatingController@create');

*/

$allowedActions = [
    'getSingle' => [
        'httpVerb' => 'get',
        'routePattern' => '<plural>/{<singular>}'
    ],
    'getList' => [
        'httpVerb' => 'get',
        'routePattern' => '<plural>',
    ],
    'create' => [
        'httpVerb' => 'post',
        'routePattern' => '<plural>',
    ],
    'update' => [
        'httpVerb' => 'put',
        'routePattern' => '<plural>/{<singular>}'
    ],
];

$modelTypes = [
    'recipes' => [
        'pluralName' => 'recipes',
        'singularName' => 'recipe',        
        'controllerName' => 'RecipeController',
        'allowedActions' => [
            'getSingle',
            'getList',
            'create',
            'update',
        ],
    ],
    'ratings' => [
        'pluralName' => 'ratings',
        'singularName' => 'rating',        
        'controllerName' => 'RatingController',
        'allowedActions' => [
            'getList',
            'create',
        ],
    ],    
];

$routeGenerator = app('\App\Helpers\RouteGenerator', ['allowedActions' => $allowedActions]);
$routeGenerator->buildRoutes($modelTypes);