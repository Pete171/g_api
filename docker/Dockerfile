FROM amd64/ubuntu AS builder

ARG PHP_VERSION=7.2.8
ARG DB_FILE=/tmp/database.prod.db
ARG GIT_USERNAME
ARG GIT_PASSWORD

SHELL ["/bin/bash", "-c"]

EXPOSE 80

RUN echo "Europe/Dublin" > /etc/timezone

ENV DEBIAN_FRONTEND noninteractive
ENV DEBCONF_NONINTERACTIVE_SEEN true

RUN export DEBIAN_FRONTEND=noninteractive \
        && apt-get update \
	&& apt-get install tzdata -y \
	&& apt-get install libssl-doc \
        && apt-get install libssl-dev -y \
	&& apt-get install libcurl4-openssl-dev -y \
	&& apt-get install php7.2 -y \
	&& apt-get install curl -y \
	&& apt-get install g++ -y \
	&& apt-get install libxml2-dev -y \
	&& apt-get install bzip2 -y \
	&& apt-get install libcurl4 -y \
	&& apt-get install libreadline-dev -y \
	&& apt-get install libxslt-dev -y \
	&& apt-get install make -y \
	&& apt-get install libbz2-dev -y \
	&& apt-get install git -y \
	&& apt-get install libsqlite3-dev -y \
	&& apt-get install autoconf -y

RUN curl -L -O https://github.com/phpbrew/phpbrew/raw/master/phpbrew \
     	&& chmod +x phpbrew \
     	&& mv phpbrew /usr/local/bin/phpbrew \
     	&& echo "[[ -e ~/.phpbrew/bashrc ]] && source ~/.phpbrew/bashrc" >> /root/.bashrc \
        && phpbrew init \
	&& phpbrew install $PHP_VERSION

RUN source ~/.phpbrew/bashrc \
        && phpbrew use 7.2.8 \
        && phpbrew switch $PHP_VERSION \
        && phpbrew ext install pdo_sqlite

RUN source ~/.phpbrew/bashrc \
       && phpbrew use 7.2.8 \
       && php -r "copy('https://getcomposer.org/installer', 'composer-setup.php');" \
       && php -r "if (hash_file('SHA384', 'composer-setup.php') === '544e09ee996cdf60ece3804abc52599c22b1f40f4323403c44d44fdfdd586475ca9813a858088ffbc1f233e9b180f061') { echo 'Installer verified'; } else { echo 'Installer corrupt'; unlink('composer-setup.php'); } echo PHP_EOL;" \
       && php composer-setup.php \
       && php -r "unlink('composer-setup.php');" \
       && mv composer.phar /usr/local/bin/composer \
       && cd /usr/local \       
       && git clone https://$GIT_USERNAME:$GIT_PASSWORD@bitbucket.org/$GIT_USERNAME/g_api.git \
       && cd g_api \
       && composer install \
       && touch $DB_FILE \
       && php artisan migrate \
       && php createSampleData.php

FROM amd64/ubuntu

SHELL ["/bin/bash", "-c"]

COPY --from=builder /root/.phpbrew/ /root/.phpbrew/
COPY --from=builder /usr/local/bin/phpbrew /usr/local/bin/phpbrew
COPY --from=builder /usr/local/bin/composer /usr/local/bin/composer
COPY --from=builder /root/.bashrc /root/.bashrc
COPY --from=builder /usr/local/g_api /usr/local/g_api
COPY --from=builder $DB_FILE $DB_FILE

RUN echo "[[ -e ~/.phpbrew/bashrc ]] && source ~/.phpbrew/bashrc" >> /root/.bash_profile

ENTRYPOINT source /root/.bash_profile \
        && phpbrew use 7.2.8 \
	&& cd /usr/local/g_api \
	&& php artisan serve --port=80 --host=127.0.0.1