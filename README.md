# Getting Started

## Project Requirements


* Linux-based distribution.  (Only developed and tested on Ubuntu 16.04.1 LTS.)
* PHP 7.2
* SQLite3
* Bash

## Environment Preparation

If possible, please [install PHPBrew](https://github.com/phpbrew/phpbrew) and then follow these instructions:

    phpbrew install 7.2.8
    phpbrew switch 7.2.8
    phpbrew ext install pdo_sqlite

Using PHPBrew is not essential.  Using the default PHP provider by OS package managers (`apt-get`, `yum`, etc.) should be OK.  Using PHPBrew just ensures the project is run with the same PHP version and extensions that it was developed with - thus reducing cross-environment incompatibilities.

## Installing the project

    git clone <repoUrl>
    cd <repoUrl>
	composer install
	
## Running The Test Suite

### Full Suite

    cd <project_root>
    vendor/bin/phpunit-randomizer --order-by=random

### Unit Tests

    vendor/bin/phpunit-randomizer tests/Unit --order-by=random

### Feature Tests

    vendor/bin/phpunit-randomizer tests/Feature --order-by=random
	
### Test Information

    The unit tests follow standard unit testing practise: full mocking of all dependencies, etc.
	
	Feature tests run an actual instance of the appliation.  Real REST API requests are sent to it and it returns real API responses that are parsed by the test runner.
	
	This sample application is too small to support separte acceptance tests: in a real project acceptance tests would indirectly test the REST API by e.g. interacting with a customer-facing website and asserting the state of the admin panel is as expected.

## Launching/Using An Example Development Server

    cd <project_root>
    php artisan migrate // A one time command: creates an empty SQLite DB in /tmp/
    php createSampleData.php // Restores the sample data in the DB: populates with the 10 recipes from the provided CSV file.  Truncates any ratings.
    php artisan serve --port=80 --host=127.0.0.1

In a separate SSH window you can now perform requests and view the responses.  E.g:

    curl 'http://127.0.0.1/api/recipes/'
    
You may wish to use the `jq` utility to parse and format the JSON response.  E.g:

    curl 'http://127.0.0.1/api/recipes/' | jq

More information about the API is given in later sections.

## PSR-2 Coding Standards Check / Auto-Fixing

Adherence to PSR-2 for the app/ and tests/ directories can be checked with:

    vendor/bin/phpcs app --standard=PSR2
    vendor/bin/phpcs tests --standard=PSR2 --exclude=PSR2.Methods.MethodDeclaration,PSR1.Methods.CamelCapsMethodName
	
Run these commands to automatically modify the app/ and test/ directories according to PSR-2:

    vendor/bin/phpcbf tests --standard=PSR2
    vendor/bin/phpcs tests --standard=PSR2
	
## Committing

Prior to committing run this script:

    cd <project_root>
	./prepareCommit.sh
	
This will run the unit tests, feature tests, and check the app/ and tests/ directory for PSR-2 compliance.

# Getting Started - Alternative

A Docker image has been created as an alternative to the above (installing PHP/PHPBrew/checking out the source code manually).  If this is of any use please ask for AWS IAM credentials.

Once authorised, to run the container:

    docker pull 082187247526.dkr.ecr.eu-west-2.amazonaws.com/pb/gousto_test
    docker run --rm -it --network host 082187247526.dkr.ecr.eu-west-2.amazonaws.com/pb/gousto_test
    
The container is listening on `127.0.0.1:80`.  In a separate SSH window requests can then be made:

    `curl '127.0.0.1/api/recipes'`

To execute Bash commands on the container run it with this:

    docker run --entrypoint /bin/bash --rm -it --network host 082187247526.dkr.ecr.eu-west-2.amazonaws.com/pb/gousto_test

You can then navigate to the project root and execute tests, as in the 'Getting Started' section:

    cd /usr/local/g_api/
    vendor/bin/phpunit tests/Unit
    vendor/bin/phpunit tests/Feature

Note - to rebuild the Docker image (for development purposes) run this command:

    docker build . --build-arg GIT_USERNAME=<git_username> --build-arg GIT_PASSWORD=$(cat <git_password_file>)

# Choice of Framework / Tools

Laravel was chosen because it is both simple to use to create REST APIs and is powerful enough to be flexible and scalable for larger projects.

SQLite was used because it would meet the functional requirements of the project and allow the use of Laravel's Eloquent ORM.  Eloquent models make CRUD operations very easy.

# Design Considerations

### Design Considerations - API - Client Usage - Request

Although the functional requirements for this project are minimal, the REST API has been designed with consistency and scalability in mind.

The endpoints exposed are:

    GET api/recipes - Retrieve all recipes
    GET api/recipes/<recipeId> - Retrieve a recipe
    POST api/recipes - Add a recipe
    PUT api/recipes/<recipeId> - Update a recipe

    GET api/ratings - Retrieve all ratings
    POST api/ratings - Add a rating

Additional endpoints would all follow this same convention (the following have not been implemented):

    POST api/ratings - Add a rating
    PUT api/rating/<ratingId> - Update a rating
	
	GET api/users - Retieve all users
	...etc.

The 'GET <object>' endpoint that retrieves a list of objects supports filtering and pagination.

When called with no arguments, all objects will be returned (perhaps in multiple pages: more is said about pagination below).

To only return objects that meet certain critera this syntax can be adopted:

    GET api/<object>?<object_key>=<object_value>

Where `<object_key>` corresponds to column names in the SQLite database and `<object_value>` is the value that objects in the database must have in order to be included in the result set.

E.g:

    GET api/recipes?cuisine=british
	
Multiple filters can be included in the same request:

    GET api/recipes?cuisine=british&season=all

Results are paginated: by default, 8 results per page are returned.  This can be altered by specifying this query parameter:

    GET api/<object>?recordsPerPage=<num>

E.g:

    GET api/recipes?recordsPerPage=20

To view a different page specify this parameter:

    GET api/<object>?page=<num>
	
E.g:

    GET api/recipes?page=2
	
The 'page' defaults to 1.

The 'recordsPerPage' and 'page' options can be combined:

    GET api/<object>?recordsPerPage=<num>&page=<num>
	
E.g:

    GET api/recipes?recordsPerPage=5&page=2

All objects can be returned in one request - i.e. pagination can be disabled - by calling:

    GET api/<object>?all=1
	
E.g:

    GET api/recipes?all=1

Even with `?all=1` set, only objects matching other filters included with the request (see earlier) will be returned.

### Design Considerations - API - Client Usage - Response

API responses have been designed with consistency in mind.

All API responses return a JSON string that, once decoded, contains this format:

    [
        'error' => <num> // Where 0 indicates no error
		'data' => [] // Where the contents inside here can change
    ]
	
This consistent response body allows consumers to reliably detect when a request was successful or was failed.

Containing all other fields from the response in the'data' property allows the API to be extensible in the future: other properties could be added alongside the 'error' and 'data' properties safely, without conflicting with any fields potentially returned by the API and parsed by consumers.

### Design Considerations - API - Software Design

`\App\Http\Controllers\Helper` has been written as a 'shared' controller file that most endpoints can use.

This contains generic logic for handling each request type supported by the API (`GET api/<objects>`, `PUT api/<object>/<objectId>` etc).

This means that most controller files are very small and contain very little logic.  See:

    app/Http/Controllers/RecipeController.php
    app/Http/Controllers/RatingController.php

Note that even if the API supported 100 different objects (e.g. user, shopping_basket, order, payment, warehouse, etc.) then each controller would still just be as small as the above two are.

Similar design has been given to the addition of the controller routes.  Normally within Laravel routes can be added by code like the following:

    Route::get('recipes/{recipe}', 'RecipeController@getSingle');
	
The above tells Laravel to execute the `RecipeController::getSingle()` method when `GET api/recipes/<recipeId>` is called, passing `<recipeId>` to the first parameter of `RecipeController::getSingle()`.

That is simple enough.  Here is what one set of routes looks like:

    Route::get('recipes/{recipe}', 'RecipeController@getSingle');
    Route::put('recipes/{recipe}', 'RecipeController@update');
    Route::get('recipes', 'RecipeController@getList');
    Route::post('recipes', 'RecipeController@create');

Now: what if this was a real REST API supporting lots of endpoints?  We might have 100 copies of the above.  This is not very nice to maintain (or change, if altering the API).

A small class has been written - `app/Helpers/RouteGenerator.php` - that accepts two arrays.  The class uses the information in these arrays to programatically build all routes supported by the API.  The two arrays are all that need to be modified to control the routing for the entire REST API.  This class is called in the `routes/api.php` file - the standard file to add API routes to in Laravel.

## Design Considerations - What Has Not Been Implemented

There are numerous notable omissions in this API that were not part of the functional requirements.  The most obvious ones that would be addressed in a real implementation are:

1. No support for DELETE requests.
2. No ability to search integer or date ranges - e.g. `GET recipes?preparation_time_minutes=btwn:0:30
3. No ability to search for integer or date ranges above or below certain values - e.g. `GET recipes?preparation_time_minutes=gte:30
4. The API response to `GET <object>` requests that lists multiple objects should return fields alongside 'error' and 'data' to tell the client if the result has been paginated / how many pages there are in total / how many rows are in the current page / how many rows there are across all pages.
5. No consideration of TLS.  (Parts of a real API may transfer PII / PCI data, etc, and so should be correctly secured.)

## Project File List

Some files in the repository are default files created by `laravel new`.  To assist with code review, the files listed are the main ones created/modified by this project:

    app/Http/Controllers/ - All files
    app/Http/Middleware/FormatApiResponse.php
    app/Http/Kernel.php
    app/Helpers/RouteGenerator.php
    app/Models/ - All files
    routes/api.php
    tests/ - All files
    docker/Dockerfile
    prepareCommit.sh
    .env